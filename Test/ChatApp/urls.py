from django.urls import path
from . import views

#template tagging
app_name = 'ChatApp'

urlpatterns = [
    path('register', views.register, name="register"),
    path('index', views.index, name="index"),
    path('', views.home, name="home"),
    path('login', views.user_login, name="user_login"),
    path('logout', views.user_logout, name="user_logout"),
]
