from django import forms
from django.contrib.auth.models import User
# from .models import UserProfileInfo


class form_user(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput())
    password2 = forms.CharField(widget=forms.PasswordInput())

    class Meta():
        model = User
        fields = ("username", "password", "password2", "email")

