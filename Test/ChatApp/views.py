from django.shortcuts import render
from .forms import form_user
from django.contrib import messages
from django.urls import reverse
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth import authenticate, login, logout


# Create your views here.


def register(request):
    registered = False
    if request.method == "POST":
        password = request.POST.get('password')
        password2 = request.POST.get('password2')
        user_form = form_user(data=request.POST)

        if password == password2:
            if user_form.is_valid():
                user = user_form.save()
                user.set_password(user.password)
                user.save()
                registered = True

            else:
                print(user_form.errors)

        else:
            print("Password Mismatch")
            return HttpResponse("Password Mismatch")

    else:
        user_form = form_user()

    return render(request, 'ChatApp/registration.html',
                  {
                      'user_form': user_form,
                      'registered': registered,
                  })


def index(request):
    return render(request, 'ChatApp/index.html')


def home(request):
    return render(request, 'ChatApp/base.html')


def user_login(request):
    if request.method == "POST":
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(username=username, password=password)

        if user:
            if user.is_active:
                login(request, user)
                # return HttpResponse("Login Successfull")
                return HttpResponseRedirect('index')
            else:
                return HttpResponse("Account not active")
        else:
            print("Someone tried to login and failed")
            print("Username: " + username + " Password: " + password)
            return HttpResponse("Invalid Login details")

    else:
        return render(request, 'ChatApp/login.html', {})


@login_required
def user_logout(request):
    logout(request)
    return HttpResponse("User Logged Out")
